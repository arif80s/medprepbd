package com.kids.crm.medically;

import com.kids.crm.model.*;
import com.kids.crm.model.mongo.QuestionSolvingTime;
import com.kids.crm.mongo.repository.QuestionSolvingTimeRepository;
import com.kids.crm.repository.QuestionRepository;
import com.kids.crm.repository.StudentAnswerRepository;
import com.kids.crm.repository.UserRepository;
import com.kids.crm.service.GraphService;
import com.kids.crm.service.UserService;
import com.kids.crm.util.Util;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.*;

import static org.reflections.util.ConfigurationBuilder.build;

@RunWith(SpringRunner.class)
@SpringBootTest
public class MedicallyApplicationTests {
	@Autowired
	UserRepository userRepository;
	@Autowired
	StudentAnswerRepository studentAnswerRepository;
	@Autowired
	GraphService graphService;
	@Autowired
	QuestionSolvingTimeRepository questionSolvingTimeRepository;
	@Autowired
	QuestionRepository questionRepository;
	@Autowired
	UserService userService;

	@Test
	public void contextLoads() {

	}
//
//	@Test
//	public void getUser(){
//
//        User user = graphService.getAuthenticateUser();
//        System.out.println(user.getEmail());
//        System.out.println(user.getFirstName());
//	}
//
//	@Test
//	public void userRepo(){
//		System.out.println(userRepository.findById(3).getEmail());
//	}
//
//	@Test
//    public void studentAnswer(){
//        User user = graphService.getAuthenticateUser();
//        List<StudentAnswer> studentAnswer = graphService.studentAnswersList(user);
//        for (int i = 0; i < studentAnswer.size(); i++){
//            StudentAnswer sa = studentAnswer.get(i);
//            System.out.println(sa.getAttendedOn());
//
//        }
//    }
//
//    @Test
//    public void getDate(){
//        System.out.println(Util.daysBack10(new Date()));
//    }
//    @Test
//    public void getGraphStudent(){
//        List<GraphStudentResult> graphStudenResults = graphService.getGraphStudentResultList();
//        System.out.println(graphStudenResults.size());
//        for (GraphStudentResult g: graphStudenResults)
//        {
//            System.out.println(g.getDate() );
//            System.out.println(g.getCorrect());
//            System.out.println(g.getWrong());
//            System.out.println("-----------------------------");
//
//        }
//    }
//    @Test
//    public void graphStudentMarks(){
//
//        List<GraphStudentMarks> graphStudentMarksList = graphService.graphStudentMarksList();
//        for (GraphStudentMarks graphStudentMarks : graphStudentMarksList
//             ) {
//            System.out.println(graphStudentMarks.getDate());
//            System.out.println(graphStudentMarks.getMarks());
//            System.out.println(graphStudentMarks.getTopic());
//        }
//    }

//    @Test
//    public void graphStudentMarksmap(){
//        System.out.println(graphService.graphStudentMarksMap().size());
//        System.out.println(graphService.graphStudentMarksMap().values());
//    }

//	@Test
//	public void findAllByUser(){
//		User user = graphService.getAuthenticateUser();
//		System.out.println(studentAnswerRepository.findAllByUser(user).size());
//	}

//	@Test
//	public void subTopic(){
//		List<StudentAnswer> studentAnswerList = studentAnswerRepository.findAllByUser(graphService.getAuthenticateUser());

// 		for (StudentAnswer studentAnswer : studentAnswerList) {
//			topic = studentAnswer.getQuestion().getTopic().getName();
//			SubTopic subTopicOb = (SubTopic)studentAnswer.getQuestion().getSubTopics();
//			subTopic = subTopicOb.getName();
//			if (studentAnswer.getQuestion().getSubTopics().toArray().length > 1){
//				System.out.println("subTopic set size : "+studentAnswer.getQuestion().getSubTopics().toArray()[1]);
//			}
//			else
//				System.out.println("subTopic set size : "+studentAnswer.getQuestion().getSubTopics().toArray()[0]);
//
//			System.out.println("Topic name : "+ topic);
//			System.out.println("Sub Topic name : "+ subTopic);
//			System.out.println("-------------------------------------------");
//
//
//		}
//
//		SubTopic subTopic;
//		String subTopicName;
//		String topicName;
//		int correct;
//		int wrong;
//
//		for (StudentAnswer studentAnswer : studentAnswerList) {
//			topicName = studentAnswer.getQuestion().getTopic().getName();
//			System.out.println("Topic name : "+ topicName);
//			for (int i=0; i < studentAnswer.getQuestion().getSubTopics().toArray().length; i++){
//				subTopic = (SubTopic) studentAnswer.getQuestion().getSubTopics().toArray()[i];
//				subTopicName = subTopic.getName();
//				System.out.println("sub topic name: "+ subTopicName);
//			}
//			System.out.println("-----------------------------------------");
//		}
//	}

//	@Test
//	public void stringConcate(){
//		String topic, subject, subject1, topicSubStr, concateStr;
//		List<StudentAnswer> studentAnswerList = studentAnswerRepository.findAllByUser(graphService.getAuthenticateUser());
//		for (StudentAnswer studentAnswer : studentAnswerList) {
//			topic = studentAnswer.getQuestion().getTopic().getName();
//			subject = studentAnswer.getSubject().getName();
//			//System.out.println(topic.substring(0, 3));
//
//
//			if (topic.equals("Math") || topic.equals("Chemistry")){
//				topicSubStr = topic.substring(0, 4);
//			}else
//				topicSubStr = topic.substring(0, 3);
//			concateStr = topicSubStr.concat("-"+subject);
//			System.out.println(concateStr);
//
//		}
//	}

//	@Test
//	public void topicSubtopicMap(){
//		Iterator it = graphService.TopicSubtopicMarksMap().entrySet().iterator();
//		while (it.hasNext()) {
//			Map.Entry pair = (Map.Entry)it.next();
//
//			int value = (int)pair.getValue();
//			System.out.println(pair.getKey().toString()+" -> "+ value );
//
//		}
//	}

//	@Test
//	public void subTOpicCOunt(){
//		Iterator it = subTOpicCOuntMap().entrySet().iterator();
//		while (it.hasNext()) {
//			Map.Entry pair = (Map.Entry)it.next();
//
//			System.out.println(((SubTopic) pair.getKey()).getName()+" -> "+ ((List) pair.getValue()).size() );
//
//		}
//	}
//
//	@Autowired
//	QuestionRepository questionRepository;
//
//	public Map<SubTopic, List<Question>> subTOpicCOuntMap(){
//		Map<SubTopic, List<Question>> questionSubMap = new HashMap<>();
//		for(Question question : questionRepository.findAll()){
//			for(SubTopic subTopic1 : question.getSubTopics()){
//				List<Question> questions = questionSubMap.getOrDefault(subTopic1, new ArrayList<>());
//				questions.add(question);
//				questionSubMap.put(subTopic1, questions);
//			}
//		}
//		return questionSubMap;
//	}

//	@Test
//	public void mapTest(){
//		HashMap<String, int[]> correctTotalSubTopicMap = new HashMap<>();
//		int[] myArray = new int[10];
//		myArray[0] = 1;
//		myArray[1] = 5;
//		myArray[2] = 6;
//		myArray[3] = 7;
//		myArray[4] = 18;
//		myArray[5] = 13;
//		correctTotalSubTopicMap.put("a",myArray);
//		System.out.println(correctTotalSubTopicMap.get("a")[5]);
//		int[] myArray1 = new int[10];
//		myArray1[1] = 6666;
//		correctTotalSubTopicMap.put("a", myArray1);
//		System.out.println(correctTotalSubTopicMap.get("a")[1]);
//		Map<String, List<Question>> questionSubMap = new HashMap<>();
//		List<Question> l = questionRepository.findAll();
//		questionSubMap.put("a", l);
//		System.out.println(questionSubMap.get("a").size());
//
//		Map<String, HashSet<Long>> putCorrectQuestionIdOnceUnderSubTopic = new HashMap<>();
//		HashSet<Long> set = new HashSet<>();
//		set.add(l.get(0).getId());
//		System.out.println(l.get(0).getId());
//		set.add(l.get(100).getId());
//		putCorrectQuestionIdOnceUnderSubTopic.put("m", set);
//
//		putCorrectQuestionIdOnceUnderSubTopic.get("m").add(l.get(88).getId());
//		for (int i =0; i< putCorrectQuestionIdOnceUnderSubTopic.get("m").toArray().length; i++)
//			System.out.println(putCorrectQuestionIdOnceUnderSubTopic.get("m").toArray()[i]);
//
//
//
//	}
//
	@Test
	public void topicMarks(){
		System.out.println(questionRepository.findAll().size());
		System.out.println(graphService.TopicWiseMarksMap().size());
	}

//	@Test
//	public  void mongoTest(){
////		QuestionSolvingTime questionSolvingTime = QuestionSolvingTime.builder()
////				.questionId(1)
////				.action("action")
////				.duration(30)
////				.createDate(new Date())
////				.userId(2))
////				.build();
////			for (int i=0; i <= 45; i++){
////				QuestionSolvingTime questionSolvingTime = new QuestionSolvingTime();
////				if (i%3==0)
////					questionSolvingTime.setAction("ANSWERED");
////				else
////					questionSolvingTime.setAction("SKIPPED");
////
////				questionSolvingTime.setCreateDate(new Date());
////				questionSolvingTime.setDuration(i);
//////				questionSolvingTime.setId("3");
////				questionSolvingTime.setQuestionId(i);
////				questionSolvingTime.setUserId(3);
////
////				questionSolvingTimeRepository.save(questionSolvingTime);
////
////			}
//		System.out.println(questionSolvingTimeRepository.findAllByUserId(3L).size());
//		System.out.println(questionSolvingTimeRepository.findAllByActionEqualsAndUserId("ANSWERED", 3L).size());
//		System.out.println(questionRepository.findAllByVersionEquals(0).size());
//		System.out.println(questionRepository.findAllByVersionEquals(1).size());
//		System.out.println(studentAnswerRepository.findAllByUserAndGotCorrectEquals(userService.getAuthenticateUser(), true).size());
//	}

//	@Test
//	public void consistencyTest(){
//		List<StudentAnswer> studentAnswerLast10DaysList = studentAnswerRepository.findByUserAndAttendedOnBetween(userService.getAuthenticateUser(),Util.daysBack10(new Date()), Util.daysBack1(new Date()));
//		System.out.println(studentAnswerLast10DaysList.size());
//		System.out.println(studentAnswerRepository.findByUserAndAttendedOnBetweenAndGotCorrectEquals(userService.getAuthenticateUser(),Util.daysBack10(new Date()), new Date(), false).size());
//		System.out.println(studentAnswerRepository.countDistinctByAttendedOnBetweenAndUser(Util.daysBack10(new Date()), new Date(), userService.getAuthenticateUser()));
//		System.out.println(studentAnswerRepository.findByUserAndAttendedOn(userService.getAuthenticateUser(), new Date()).size());
//
//		if (studentAnswerRepository.findByUserAndAttendedOnBetweenAndGotCorrectEquals(userService.getAuthenticateUser(),Util.daysBack10(new Date()), new Date(), false).size() >= 5){
//			System.out.println("accuracy");
//		}


//		for(int i=1; i <=10; i++){
//			Calendar cal = Calendar.getInstance();
//			Calendar cal1 = Calendar.getInstance();
//			cal.setTime(new Date());
//			cal.set(Calendar.HOUR_OF_DAY, 23);
//			cal.set(Calendar.MINUTE, 59);
//			cal.set(Calendar.SECOND, 59);
//			cal.add(Calendar.DAY_OF_MONTH, -(i+1));
//
//			cal1.set(Calendar.HOUR_OF_DAY, 23);
//			cal1.set(Calendar.MINUTE, 59);
//			cal1.set(Calendar.SECOND, 59);
//			cal1.add(Calendar.DAY_OF_MONTH, -i);
//			System.out.println("cal -> "+cal.getTime());
//			System.out.println("cal1 -> "+cal1.getTime());
//			List<StudentAnswer> studentAnswerList = studentAnswerRepository.findByUserAndAttendedOnGreaterThanAndAttendedOnLessThanEqual(userService.getAuthenticateUser(), cal.getTime(), cal1.getTime());
//			for (StudentAnswer studentAnswer : studentAnswerList) {
//				System.out.println(studentAnswer.getAttendedOn()+"---");
//			}
//		}

//		List<StudentAnswer> studentAnswerList = studentAnswerRepository.findAllByUserAndAttendedOnGreaterThanAndAttendedOnLessThanEqual(userService.getAuthenticateUser(),Util.daysBack10(new Date()), Util.today());
//		for (StudentAnswer studentAnswer : studentAnswerList) {
//			System.out.println(studentAnswer.getAttendedOn());
//		}
//		System.out.println(studentAnswerList.size());
//		List<StudentAnswer> studentAnswerList = studentAnswerRepository.findAllByUserAndAttendedOnGreaterThanAndAttendedOnLessThanEqualAndGotCorrectEquals(
//				userService.getAuthenticateUser(),Util.daysBack10(new Date()),Util.today(), true);
//		for (StudentAnswer studentAnswer : studentAnswerList) {
//			System.out.println(studentAnswer.getId());
//		}
//		System.out.println(studentAnswerList.size());
//
//	}




}

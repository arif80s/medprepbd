<html>
<head>

    <style>


        /*.box {*/
            /*float: left;*/
            /*width: 33.33%;*/
            /*padding: 50px;*/
        /*}*/

        #liquid::after {
            content: "";
            clear: both;
            /*display: table;*/
        }
    </style>


</head>
<body>

Hi ${authenticatedUser.getName()}

<canvas id="barChart" width="350" height="275"></canvas>
<canvas id="studentDailyRightWrong" width="350" height="275"></canvas>

<div>
<canvas id="horizontalChart" width="700" height="400" style="float: left"></canvas>
<div id="test" style = " height: 536px; width: 172px; background-color: powderblue; margin-right: 395px; float: right"></div>
</div>
<#--<div id="sideBar" style = " height: 439px; width: 172px; background-color: powderblue; margin-left: 100px;">-->
<#--<div style = " height: 40px; width: 172px; background-color: green;">-->
<#--<h5>School Engagement Overview</h5>-->
<#--</div>-->
<#--<div class ="suboverview" style="height: 300px; width: 250px; background-color: black;"><p><span id="phy" style = "	color: green; font-size: 50px;"> 6% </span></br> Physics</P></div>-->
<#--<div class ="suboverview" style="height: 300px; width: 250px; background-color: black;"><p><span id ="eng" style = "	color: yellow; font-size: 50px;"> 4% </span></br> English</P></div>-->
<#--<div class ="suboverview" style="height: 300px; width: 250px; background-color: black;"><p><span id="math" style = "	color: red; font-size: 50px;"> 41%</span> </br> Math</P></div>-->
<#--<div class ="suboverview" style="height: 300px; width: 250px; background-color: black;"><p> <span id="total" style = "	color: grey; font-size: 50px;">51%</span> </br> Total</P></div>-->

<#--</div>-->


<div>
    <table width=50% >
        <thead>
            <tr id="marksTableHead"></tr>
        </thead>
        <tbody id="marksTableBody">

        </tbody>
    </table>
</div>

<div>
    <div style="float: left">
        <svg id="fillgauge" width="97%" height="250" onclick="gauge1.update(NewValue());"></svg>
        <p id="total" style="margin-left: 610px">total</p>
    </div>
    <div id="liquid" style="width: 1000px; height: 200px;">

    </div>
</div>

<canvas id="radar-chart" width="800" height="600"></canvas>


<script src="/js/jquery.js"></script>
<script src="/js/Chart.js"></script>
<script src="http://d3js.org/d3.v3.min.js" language="JavaScript"></script>
<script src="/js/liquidFillGauge.js" language="JavaScript"></script>
<script src="/js/secondMain.js"></script>

</body>

</html>
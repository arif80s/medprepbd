



var arr, body, tab, tr, td, tn, row, col, th;
arr = [
    ['heading1', 'heading2', 'heading3', "heading4", "heading5", "heading6", "heading7", "heading8"],
    [53+'%',34+'%',66+'%',55+'%',72+'%',100+'%',92+'%',71+'%'],
    [23+'%',54+'%',57+'%',89+'%',43+'%',23+'%',89+'%',81+'%'],
    [54+'%',67+'%',76+'%',88+'%',83+'%',100+'%',24+'%',22+'%'],
    [46+'%',100+'%',100+'%',65+'%',82+'%',95+'%',92+'%',71+'%'],
    [23+'%',45+'%',75+'%',55+'%',72+'%',73+'%',23+'%',43+'%'],
    [23+'%',77+'%',66+'%',37+'%',94+'%','-',92+'%',71+'%'],
    [53+'%',34+'%',66+'%',55+'%',72+'%',100+'%',92+'%',71+'%'],
    ['-',34+'%','-',55+'%',72+'%','-','-','-'],
    ['-',34+'%',66+'%','-',72+'%',100+'%','-',71+'%'],
    [53+'%',34+'%',66+'%',55+'%',72+'%',100+'%',92+'%',71+'%'],
];
body = document.getElementsByTagName('body')[0];
tab = document.createElement('table');
th = document.createElement('th');
for (row = 0; row < arr.length; row++){
    if (row == 0) {
        th = document.createElement('th');
        for (col = 0; col < arr['th'].length; col++){
            td = document.createElement('td');
            tn = document.createTextNode(arr[row][col]);
            td.appendChild(tn);
            th.appendChild(td);
        }
        tab.appendChild(th);
    }
    else {

    tr = document.createElement('tr');
    for (col = 0; col < arr[row].length; col++){

        td = document.createElement('td');
        tn = document.createTextNode(arr[row][col]);
        td.appendChild(tn);
        tr.appendChild(td);

    }
    tab.appendChild(tr);
    }

}
body.appendChild(tab);


var allTableCells = document.getElementsByTagName("td");

for(var i = 0, max = allTableCells.length; i < max; i++) {
    var node = allTableCells[i];

    //get the text from the first child node - which should be a text node
    var currentText = node.childNodes[0].nodeValue;
    var num = currentText.match(/\d+/g);

    //check for 'one' and assign this table cell's background color accordingly
    if (num < 50 && num >=1 )
        node.style.backgroundColor = "red";
    else if (num >= 50 && num < 80 )
        node.style.backgroundColor = "yellow";
    else if (num >= 80 && num <= 100 )
        node.style.backgroundColor = "green";
    else
        node.style.backgroundColor = "white";}
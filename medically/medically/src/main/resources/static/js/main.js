

/*


$(document).ready(function(){
    $.ajax({
        method: "GET",
        url: "http://localhost:8794/bar/"
    }).done(function( json ) {
        var ctx = document.getElementById("myChart").getContext('2d');
        var myChart = new Chart(ctx, {
            type: 'bar',
            data: {
                labels: json.barChartLabels,
                datasets: [{
                    label: '# of Votes',
                    data: json.barChartData,
                    backgroundColor: [
                        '#228B22',
                        '#0000FF',
                        'rgba(255, 206, 86, 0.2)',
                        'rgba(75, 192, 192, 0.2)',
                        'rgba(153, 102, 255, 0.2)',
                        'rgba(255, 159, 64, 0.2)'
                    ],
                    borderColor: [
                        'rgba(255,99,132,1)',
                        'rgba(54, 162, 235, 1)',
                        'rgba(255, 206, 86, 1)',
                        'rgba(75, 192, 192, 1)',
                        'rgba(153, 102, 255, 1)',
                        'rgba(255, 159, 64, 1)'
                    ],
                    borderWidth: 1
                }]
            },
            options: {
                responsive: false,
                scales: {
                    yAxes: [{
                        ticks: {
                            beginAtZero:true
                        }
                    }]
                }
            }
        });

    });
});
*/

$(document).ready(function(){
    $.ajax({
        method: "GET",
        url: "http://localhost:8794/correctWrong/"
    }).done(function( json ) {
        var ctx = document.getElementById("correctWrong").getContext('2d');
        var chart = new Chart(ctx, {
            type: 'bar',
            data: {
               // labels: [new Date().getDate()+'th mar', new Date().getDate()+1+'th march', new Date().getDate()+2+ 'th march', new Date().getDate()+3+ 'th march', new Date().getDate()+4+'th march'], // responsible for how many bars are gonna show on the chart
                labels: json.date,
                datasets: [{
                    label: 'Correct',
                   // data: [80, 60, 19, 0, 50],
                    data: json.correct,
                    backgroundColor: 'aqua'
                }, {
                    label: 'Wrong',
                   // data: [20, 17, 10, 0, 10],
                    data: json.wrong,
                    backgroundColor: '#FFB6C1'
                }
                ]
            },
            options: {
                responsive: false,
                legend: {
                    position: 'left' // place legend on the right side of chart
                },
                scales: {
                    xAxes: [{
                        stacked: true // this should be set to make the bars stacked
                    }],
                    yAxes: [{
                        stacked: true // this also..
                    }]
                }
            }
        });

    });
});


/*

var ctx = document.getElementById("lineChart").getContext('2d');
var chart = new Chart(ctx, {
    // The type of chart we want to create
    type: 'line',

    // The data for our dataset
    data: {
        labels: ["January", "February", "March", "April", "May", "June", "July"],
        datasets: [{
            label: "My First dataset",
            backgroundColor: 'rgb(255, 99, 132)',
            borderColor: 'rgb(255, 99, 132)',
            data: [0, 10, 5, 2, 20, 30, 45],
        }]
    },

    // Configuration options go here
    options: {
        responsive: false,
    }
});*/

/*$(document).ready(function() {
    var lt = $("#abc").data
    {
        "test"
    }
    ;
    console.log(lt);
});*/

var chart = new Chart(verticalst, {
    type: 'bar',
    data: {

        labels: [new Date().getDate()+'th mar', new Date().getDate()+1+'th march', new Date().getDate()+2+ 'th march', new Date().getDate()+3+ 'th march', new Date().getDate()+4+'th march'], // responsible for how many bars are gonna show on the chart
        // create 12 datasets, since we have 12 items
        // data[0] = labels[0] (data for first bar - 'Standing costs') | data[1] = labels[1] (data for second bar - 'Running costs')
        // put 0, if there is no data for the particular bar
        datasets: [{
            label: 'Correct',
            data: [80, 60, 19, 0, 50],
            backgroundColor: 'aqua'
        }, {
            label: 'Wrong',
            data: [20, 17, 10, 0, 10],
            backgroundColor: '#FFB6C1'
        }
        ]
    },
    options: {
        responsive: false,
        legend: {
            position: 'left' // place legend on the right side of chart
        },
        scales: {
            xAxes: [{
                stacked: true // this should be set to make the bars stacked
            }],
            yAxes: [{
                stacked: true // this also..
            }]
        }
    }
});
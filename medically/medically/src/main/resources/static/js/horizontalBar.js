
var chartColors = {
    red: 'rgb(255, 99, 132)',
    orange: 'rgb(255, 159, 64)',
    yellow: 'rgb(255, 205, 86)',
    green: 'rgb(75, 192, 192)',
    blue: 'rgb(54, 162, 235)',
    purple: 'rgb(153, 102, 255)',
    grey: 'rgb(231,233,237)',
};
new Chart(horizontalChart, {
    type: 'horizontalBar',
    data: {
        labels: ["phy-wave", "phy-magnitude", "phy-speed", "Eng-writing", "Eng-reading","Eng-spoking", "Eng-listening", "Math-aljebra", "Math-geometry", "Math-Inegration","Math-diff", "Math-Matrix", "Chem-reaction", "Chem-polymer", "Chem-isomerism"],
        datasets: [ {
            label: "total",
            fill: true,
            //backgroundColor: "rgba(1,181,10,0.2)",
            backgroundColor: ['#ffddaa', '#ffddaa','#ffddaa','#ffddff','#ffddff','#ffddff','#ffddff','#aaaaff','#aaaaff','#aaaaff','#aaaaff','#aaaaff',],
            borderColor: "rgba(179,181,198,1)",
            pointBorderColor: "#fff",
            pointBackgroundColor: "rgba(179,181,198,1)",
            data: [8,55,21,6,6,8 ,55 ,21 ,6 ,6 ,8 ,55 ,21 ,6 ,6 ]
        }],
    },
    options: {

        responsive: false,
        legend: {
            display: false,
            labels: {
                padding: 20,

            }
        },
        scales: {
            xAxes: [{
                ticks: {

                    beginAtZero: true,
                },
                gridLines: {
                    display: true,
                    lineWidth: 1,
                    //drawTicks: false,
                    //drawBorder: false,
                    zeroLineWidth: 1,


                },

            }],
            yAxes: [{
                barPercentage: .9, // I've also tried all from [0.90, 1]
                categoryPercentage: .9, // I've also tried all from [0.90, 1]
                ticks: {
                    padding: 25,

                },
            }]
        },
    },
});

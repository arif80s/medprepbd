//bar chart
$(document).ready(function(){
    $.ajax({
        method: "GET",
        url: "http://localhost:8794/graph/bar/data"
    }).done(function( json ) {
        var ctx = document.getElementById("barChart").getContext('2d');
        var myChart = new Chart(ctx, {
            type: 'bar',
            data: {
                labels: json.barChartLabels,
                datasets: [{
                    label: '# of Votes',
                    data: json.barChartData,
                    backgroundColor: [
                        '#228B22',
                        '#0000FF',
                        'rgba(255, 206, 86, 0.2)',
                        'rgba(75, 192, 192, 0.2)',
                        'rgba(153, 102, 255, 0.2)',
                        'rgba(255, 159, 64, 0.2)'
                    ],
                    borderColor: [
                        'rgba(255,99,132,1)',
                        'rgba(54, 162, 235, 1)',
                        'rgba(255, 206, 86, 1)',
                        'rgba(75, 192, 192, 1)',
                        'rgba(153, 102, 255, 1)',
                        'rgba(255, 159, 64, 1)'
                    ],
                    borderWidth: 1
                }]
            },
            options: {
                responsive: false,
                scales: {
                    yAxes: [{
                        ticks: {
                            beginAtZero:true
                        }
                    }]
                }
            }
        });

    });
});

//Student daily right/wrong
$(document).ready(function(){
    $.ajax({
        method: "GET",
        url: "http://localhost:8794/graph/studentDailyRightWrong/data"
    }).done(function( json ) {
        var ctx = document.getElementById("studentDailyRightWrong").getContext('2d');
        var chart = new Chart(ctx, {
            type: 'bar',
            data: {

                labels: json.map(function(el) {
                    return el.date
                }),
                // create 12 datasets, since we have 12 items
                // data[0] = labels[0] (data for first bar - 'Standing costs') | data[1] = labels[1] (data for second bar - 'Running costs')
                // put 0, if there is no data for the particular bar
                datasets: [{
                    label: 'Correct',
                    data: json.map(function(el) {
                        return el.correct
                    }),
                    backgroundColor: 'aqua'
                }, {
                    label: 'Wrong',
                    data: json.map(function(el) {
                        return el.wrong
                    }),
                    backgroundColor: '#FFB6C1'
                }
                ]
            },
            options: {
                responsive: false,
                legend: {
                    position: 'left' // place legend on the right side of chart
                },
                scales: {
                    xAxes: [{
                        stacked: true // this should be set to make the bars stacked
                    }],
                    yAxes: [{
                        stacked: true, // this also..
                        ticks: {
                            max: 100
                        }
                    }]
                }
            }
        });

    });
});

$(document).ready(function(){
    $.ajax({
        method: "GET",
        url: "http://localhost:8794/graph/html/data"
    }).done(function( json ) {
        var topics = {
            'Gen-Forces' : [],
            'Gen-Motion' : [],
            'Ele-Current' : [],
            'Ele-Potential Difference': [],
            'The-Pressure Changes' : [],
            'Wav-Wave Properties' : [],
            'Gen-Energy' : [],
            'Gen-Work' : [],
            'The-Radiation' : [],
            'Wav-Sound' : [],
            'Ato-Half Life' : [],
            'Ato-Decay' : [],
            'Ato-Isotopes' : [],
            'Ato-Emission' : [],
            'The-Convection' : [],
            'Gen-Length' : [],
            'Gen-Acceleration' : [],
            'The-Evaporation' : [],
            'Gen-Pressure' : [],
        };
         var dates = Object.keys(json);
        var daywiseTopic = Object.values(json);
         var topic_names = Object.keys(topics);
         for (var i = 0; i < daywiseTopic.length; i++)
        {
            for (var j = 0; j < topic_names.length; j++)
            {
                topics[topic_names[j]].push(typeof daywiseTopic[i][topic_names[j]] === 'undefined' ? '-' : daywiseTopic[i][topic_names[j]]);
            }
        }


        $('#marksTableHead').append('<td></td>');
        for (var i=0; i < dates.length; i++)
        {
            $('#marksTableHead').append('<td>' + new Date(dates[i]) + '</td>');
        }

        var marks_table_body = $('#marksTableBody');
        for (var i = 0; i < topic_names.length; i++)
        {
            var table_row = '<tr>';
            table_row += '<td>' + topic_names[i] + '</td>';
            for (var j=0; j < dates.length; j++)
            {
                console.log(topics[topic_names[i]][j]);
                //table_row += '<td style="background-color: blue">' + topics[topic_names[i]][j] + '</td>';
                if (topics[topic_names[i]][j] < 50 && topics[topic_names[i]][j] >= 0)
                    table_row += '<td style="background-color: red">' + topics[topic_names[i]][j] + '</td>';
                else if (topics[topic_names[i]][j] < 80 && topics[topic_names[i]][j] >= 50)
                    table_row += '<td style="background-color: yellow">' + topics[topic_names[i]][j] + '</td>';
                else if (topics[topic_names[i]][j] <= 100 && topics[topic_names[i]][j] >= 80)
                    table_row += '<td style="background-color: green">' + topics[topic_names[i]][j] + '</td>';
                else
                    table_row += '<td style="background-color: navajowhite">' + topics[topic_names[i]][j] + '</td>';
            }
            table_row += '</tr>';
            $('#marksTableBody').append(table_row);
        }


    });
});


// side bar
$(document).ready(function(){
    $.ajax({
        method: "GET",
        url: "http://localhost:8794/graph/sideBar/data"
    }).done(function( json ) {
        $('#test').append(' <div style = " height: 40px; width: 172px; background-color: green; margin-top: -21px;">'+
            '<h5>'+'School Engagement Overview'+'</h5>'+
       ' </div>'+
        '<div  style=" padding-left: 46px; "><p><span id="phy" style = "color: green; font-size: 50px; text-align: center;">'+Object.values(json)[0]+'</span></br><span     text-align: center;> '+ Object.keys(json)[0]+'</span></P></div>'+
        '<div   style=" padding-left: 46px; "><p><span id ="eng" style = "color: yellow; font-size: 50px;     text-align: center;"> '+Object.values(json)[1]+'</span></br><span    text-align: center;>'+ Object.keys(json)[1]+'</span></P></div>'+
        '<div   style=" padding-left: 46px; "><p><span id="math" style = "color: red; font-size: 50px;    text-align: center;"> '+Object.values(json)[2]+'</span> </br> <span    text-align: center;>'+Object.keys(json)[2]+'</span></P></div>'+
        '<div   style=" padding-left: 46px; "><p><span id="math" style = "color: blue; font-size: 50px;    text-align: center;"> '+Object.values(json)[3]+'</span> </br> <span     text-align: center;>'+Object.keys(json)[3]+'</span>></P></div>'+
        '<div   style=" padding-left: 46px; "><p> <span id="total" style = "color: grey; font-size: 50px;    text-align: center;">'+(Object.values(json)[0]+Object.values(json)[1]+Object.values(json)[2]+Object.values(json)[3])+'</span> </br><span    text-align: center;>total</span></P></div>');

    });
});


//Student daily right/wrong
$(document).ready(function(){
    $.ajax({
        method: "GET",
        url: "http://localhost:8794/graph/studentSubTopicMarks/data"
    }).done(function( json ) {
        var chartColors = {
            red: 'rgb(255, 99, 132)',
            orange: 'rgb(255, 159, 64)',
            yellow: 'rgb(255, 205, 86)',
            green: 'rgb(75, 192, 192)',
            blue: 'rgb(54, 162, 235)',
            purple: 'rgb(153, 102, 255)',
            grey: 'rgb(231,233,237)',
        };

        //var keys = Object.keys(json);


        new Chart(horizontalChart, {
            type: 'horizontalBar',
            data: {
                // labels: ["phy-wave", "phy-magnitude", "phy-speed", "Eng-writing", "Eng-reading","Eng-spoking", "Eng-listening", "Math-aljebra", "Math-geometry", "Math-Inegration","Math-diff", "Math-Matrix", "Chem-reaction", "Chem-polymer", "Chem-isomerism"],
               labels: Object.keys(json),
                datasets: [ {
                    label: "total",
                    fill: true,
                    //backgroundColor: "rgba(1,181,10,0.2)",
                    backgroundColor: ['#ffddaa', '#ffddaa','#ffddaa','#ffddff','#ffddff','#ffddff','#ffddff','#aaaaff','#aaaaff','#aaaaff','#aaaaff','#aaaaff',],
                    borderColor: "rgba(179,181,198,1)",
                    pointBorderColor: "#fff",
                    pointBackgroundColor: "rgba(179,181,198,1)",
                    // data: [8,55,21,6,6,8 ,55 ,21 ,6 ,6 ,8 ,55 ,21 ,6 ,6 ]
                    data: Object.values(json),
               }],

            },
            options: {

                responsive: false,
                legend: {
                    display: false,
                    labels: {
                        padding: 20,

                    }
                },
                scales: {
                    xAxes: [{
                        ticks: {

                            beginAtZero: true,
                        },
                        gridLines: {
                            display: true,
                            lineWidth: 1,
                            //drawTicks: false,
                            //drawBorder: false,
                            zeroLineWidth: 1,


                        },

                    }],
                    yAxes: [{
                        barPercentage: .9, // I've also tried all from [0.90, 1]
                        categoryPercentage: .9, // I've also tried all from [0.90, 1]
                        ticks: {
                            padding: 25,

                        },
                    }]
                },
            },
        });


    });
});


//liquid js for topic marks
$(document).ready(function(){
    $.ajax({
        method: "GET",
        url: "http://localhost:8794/graph/sideBar/data"
    }).done(function( json ) {


        var topicSize = Object.keys(json).length;
        var totalMarks = 0;
        for (var j=0; j<topicSize; j++){
            totalMarks += Object.values(json)[j];
        }
        var gauge = loadLiquidFillGauge("fillgauge", totalMarks);

        for (var i=0; i < topicSize; i++){

            $('#liquid').append(' <div style="float: left;">\n' +
                '        <svg id="fillgauge'+i+'" width="300px" height="180px" onclick="gauge2.update(NewValue());"></svg>\n' +
                '        <p  <!-- style="margin-left: 610px; float: left "-->>'+Object.keys(json)[i]+' </p>\n' +
                '    </div>');
           var gauge2 = loadLiquidFillGauge("fillgauge"+i, Object.values(json)[i]);

        }

        function NewValue(){
            if(Math.random() > .5){
                return Math.round(Math.random()*100);
            } else {
                return (Math.random()*100).toFixed(1);
            }
        }

    });
});

//radar graph

$(document).ready(function(){
    $.ajax({
        method: "GET",
        url: "http://localhost:8794/graph/radar"
    }).done(function( json ) {
        new Chart(document.getElementById("radar-chart"), {
            type: 'radar',
            data: {
                labels: Object.keys(json),
                datasets: [
                    {
                        label: "total",
                        fill: true,
                        backgroundColor: "rgba(179,181,198,0.2)",
                        borderColor: "rgba(179,181,198,1)",
                        pointBorderColor: "#fff",
                        pointBackgroundColor: "rgba(179,181,198,1)",
                        data: [8.77,55.61,21.69,6.62,6.82]
                    }, {
                        label: "monthly",
                        fill: true,
                        backgroundColor: "rgba(255,99,132,0.2)",
                        borderColor: "rgba(255,99,132,1)",
                        pointBorderColor: "#fff",
                        pointBackgroundColor: "rgba(255,99,132,1)",
                        pointBorderColor: "#fff",
                        data: Object.values(json),
                    },{
                        label: "weekly",
                        fill: true,
                        backgroundColor: "rgba(25,99,12,0.2)",
                        borderColor: "rgba(55,99,132,199)",
                        pointBorderColor: "#cff",
                        pointBackgroundColor: "rgba(285,99,232,1)",
                        pointBorderColor: "#cff",
                        data: [2.48,54.16,-55.61,8.06,48.45]
                    }
                ]
            },
            options: {
                responsive: false,
                title: {
                    display: true,
                    text: 'radar/spider testing'
                }
            }
        });

    });
});
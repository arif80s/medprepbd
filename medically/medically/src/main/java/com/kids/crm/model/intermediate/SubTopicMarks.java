package com.kids.crm.model.intermediate;

import lombok.*;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class SubTopicMarks {
    private  String Topic;
    private String subTopic;
    private  int marks;

}

package com.kids.crm.controller;

import com.kids.crm.model.FlagMessageType;
import com.kids.crm.model.GraphStudentResult;
import com.kids.crm.model.Role;
import com.kids.crm.model.User;
import com.kids.crm.model.mongo.QuestionStats;
import com.kids.crm.mongo.repository.QuestionStatRepository;
import com.kids.crm.repository.UserRepository;
import com.kids.crm.service.GraphService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Controller
@RequestMapping("/test")
public class TestController {
    @Autowired
    UserRepository userRepository;
    @Autowired
    GraphService graphService;
    @Autowired
    QuestionStatRepository questionStatRepository;

    //user testing purpose
    @GetMapping("/userTest")
    public String user(){
        User user = new User();
        user.setRole(Role.STUDENT);
        user.setFirstName("asdf");
        user.setEmail("asd@gmail.com");
        userRepository.save(user);

        return "user saved";
    }

    @GetMapping("/printViewPage")
    // @ResponseBody
    public String passParametersWithModelMap(ModelMap map) {
        map.addAttribute("welcomeMessage", "welcome");
        map.addAttribute("message", "Baeldung");
        List<String> myList = new ArrayList<>();
        myList.add("sss");
        myList.add("dsfsdf");
        myList.add("fddffff");
        myList.add("sfdsf");
        map.addAttribute(  "mylists",   myList);
        List<GraphStudentResult> graphStudenResults = graphService.getGraphStudentResultList();
        map.addAttribute("graphStuListt", graphStudenResults );
        return "viewPage";
    }

//    @GetMapping("/save")
//    @ResponseBody
//    public String saveToDB(){
//        Map<String, Integer> answerCountWithOption = new HashMap<>();
//        answerCountWithOption.put("A", 5);
//        answerCountWithOption.put("B", 24);
//        answerCountWithOption.put("C", 52);
//        answerCountWithOption.put("D", 15);
//
//        Map<FlagMessageType, Integer> flagMessage = new HashMap<>();
//        flagMessage.put(FlagMessageType.ANSWER_NOT_HERE, 53);
//        flagMessage.put(FlagMessageType.QUESTION_IS_WRONG, 15);
//        flagMessage.put(FlagMessageType.ANSWER_NOT_HERE, 4);
//        flagMessage.put(FlagMessageType.QUESTION_IS_WRONG, 0);
//
//
////        QuestionStats questionStats = QuestionStats.builder()
////                .timesAnsweredCount(20)
////                .answeredCountWithOption(answerCountWithOption)
////                .questionId("" + 1)
////                .skipCount(12)
////                .flagMessageCount(flagMessage)
////                .build();
//
//        questionStatRepository.save(questionStats);
//
//        return "Saved";
//    }

    //testing purpose
    @GetMapping("/testing")
    public String verticalStack(ModelMap modelMap){
        modelMap.addAttribute("studentId",3);
        return "correctWrong";
    }
    @GetMapping("/horizontalBar")
    public String horizontal(){
        return "horizontalBar";
    }

}

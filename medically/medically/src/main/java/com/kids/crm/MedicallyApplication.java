package com.kids.crm;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;

@SpringBootApplication
@EnableJpaRepositories(basePackages = "com.kids.crm.repository")
@EnableMongoRepositories(basePackages = "com.kids.crm.mongo.repository")
public class MedicallyApplication {

	public static void main(String[] args) {
		SpringApplication.run(MedicallyApplication.class, args);
	}
}

package com.kids.crm.repository;

import com.kids.crm.model.Batch;
import org.springframework.data.jpa.repository.JpaRepository;

public interface BatchRepository extends JpaRepository<Batch, Long> {
    Batch findBySubjectIdAndSessionId(long subjectId, long sessionId);
}

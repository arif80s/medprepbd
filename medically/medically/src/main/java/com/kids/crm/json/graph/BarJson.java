package com.kids.crm.json.graph;

import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class BarJson {
    public List<Integer> barChartData;
    public List<String> barChartLabels;
}

package com.kids.crm.model;

import lombok.*;

import java.time.LocalDate;
import java.util.Date;
@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class GraphStudentMarks{
    private LocalDate date;
    private String topic;
    private int marks;
}

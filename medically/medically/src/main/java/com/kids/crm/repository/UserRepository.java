package com.kids.crm.repository;

import com.kids.crm.model.User;
import org.springframework.data.repository.CrudRepository;
//import org.springframework.stereotype.Repository;


public interface UserRepository extends CrudRepository <User, Long> {
/*    Optional<User> findByEmail(String email);*/
    User findById(long id);




}

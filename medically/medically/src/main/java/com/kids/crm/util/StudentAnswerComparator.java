package com.kids.crm.util;

import com.kids.crm.model.StudentAnswer;
import org.apache.commons.lang3.builder.CompareToBuilder;

import java.util.Comparator;

public class StudentAnswerComparator implements Comparator<StudentAnswer> {
    @Override
    public int compare(StudentAnswer o1, StudentAnswer o2) {
        return new CompareToBuilder()
                .append(o1.getAttendedOn(), o2.getAttendedOn())
                .append(o1.getQuestion().getTopic().getName(), o2.getQuestion().getTopic().getName()).toComparison();
     }
}

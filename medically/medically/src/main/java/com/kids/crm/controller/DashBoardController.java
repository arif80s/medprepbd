package com.kids.crm.controller;

import com.kids.crm.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class DashBoardController {

    @Autowired
    UserService userService;

    @GetMapping("/dashboard")
    public String dashboardIndex(ModelMap modelMap){
        modelMap.addAttribute("authenticatedUser", userService.getAuthenticateUser());
        return "dashboard";
    }
}

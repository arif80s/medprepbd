package com.kids.crm.model.intermediate;

import com.kids.crm.model.Question;
import lombok.*;

import java.util.Date;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class QuestionMarks {
    private Date date;
    private int marks;
    private Question question;
}

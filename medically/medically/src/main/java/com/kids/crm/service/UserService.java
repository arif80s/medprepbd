package com.kids.crm.service;

import com.kids.crm.model.User;
import com.kids.crm.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserService
{
    @Autowired
    UserRepository userRepository;

    public User getAuthenticateUser()
    {
        return userRepository.findById(3);
    }
}

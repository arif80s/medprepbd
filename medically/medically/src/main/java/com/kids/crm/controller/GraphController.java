package com.kids.crm.controller;

import com.kids.crm.json.graph.BarJson;
import com.kids.crm.model.GraphStudentResult;
import com.kids.crm.mongo.repository.QuestionStatRepository;
import com.kids.crm.repository.UserRepository;
import com.kids.crm.service.GraphService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.persistence.criteria.CriteriaBuilder;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Controller
@RequestMapping("/graph")
public class GraphController {
    @Autowired
    QuestionStatRepository questionStatRepository;

    @Autowired
    UserRepository userRepository;
    @Autowired
    GraphService graphService;



    @GetMapping("/marks")
    @ResponseBody
    public HashMap<Date, HashMap<String, Integer>> table(){
        return graphService.graphStudentMarksMap();
    }


    //user correct wrong
    @GetMapping("/studentDailyRightWrong/data")
    @ResponseBody
    public List<GraphStudentResult> correctWrong(){
        return graphService.getGraphStudentResultList();
    }

    @GetMapping("/studentSubTopicMarks/data")
    @ResponseBody
    public HashMap<String, Integer> subtopicWiseQuestionMarks(){
        return  graphService.TopicSubtopicMarksMap();
    }

    @GetMapping("/sideBar/data")
    @ResponseBody
    public HashMap<String, Integer> topicWiseMarks(){
        return graphService.TopicWiseMarksMap();
    }

    @GetMapping("/bar/data")
    @ResponseBody
    public BarJson barGraph(){
        BarJson barJson = new BarJson();
        barJson.setBarChartData(List.of(12, 19, 3, 5, 2, 883));
        barJson.setBarChartLabels(List.of("Red", "Blue", "Yellow", "Green", "Purple", "Orange"));
        return barJson;
    }

    @GetMapping("/radar")
    @ResponseBody
    public HashMap<String, Integer> radarGraph(){
        return graphService.spiderServiceMap();
    }

    @GetMapping("/html/data")
    @ResponseBody
    public HashMap<Date, HashMap<String, Integer>> htmlTableMap(){
        return graphService.htmlTableMap();
    }


}

package com.kids.crm.model;

import lombok.Getter;


@Getter
public enum Role{
    TEACHER("ROLE_TEACHER"),
    STUDENT("ROLE_STUDENT"),
    ASSISTANT("ROLE_ASSISTANT"),
    SUPER_ADMIN("ROLE_SUPER_ADMIN");

    private String name;

    Role(String name) {
        this.name = name;
    }



    public boolean isStudent() {
        return this == STUDENT;
    }

    public boolean isTeacher() {
        return this == TEACHER;
    }

    public boolean isAssistant() {
        return this == ASSISTANT;
    }

    public boolean isSuperAdmin() {
        return this == SUPER_ADMIN;
    }

    public String getNameStripped() {
        return getName().substring(5);
    }
}

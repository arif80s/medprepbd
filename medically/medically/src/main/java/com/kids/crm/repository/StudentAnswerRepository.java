package com.kids.crm.repository;

import com.kids.crm.model.Batch;
import com.kids.crm.model.ExamType;
import com.kids.crm.model.StudentAnswer;
import com.kids.crm.model.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Temporal;
import org.springframework.data.repository.Repository;

import javax.persistence.TemporalType;
import java.util.Date;
import java.util.List;

public interface StudentAnswerRepository extends Repository<StudentAnswer, Long> {

//    List<StudentAnswer> findByUserAndAttendedOnBetweenAndExamType(User user, Date from, Date to, ExamType dailyExam);
//    List<StudentAnswer> findByUserAndAttendedOnBetweenAndBatchAndExamType(User user, Date from, Date to, Batch batch, ExamType dailyExam);
//    List<StudentAnswer> findByattendedOn(@Temporal(TemporalType.DATE) Date date);
//    void removeByUserIdAndBatchId(long userId, long batchId);
//    List<StudentAnswer> findByUser(User user);

    List<StudentAnswer> findByUserAndAttendedOnBetween(User user, Date from, Date to);
    List<StudentAnswer> findAllByUserAndAttendedOnBetween(User user, Date from, Date to);
    List<StudentAnswer> findAllByUser(User user);
    List<StudentAnswer> findAllByUserAndGotCorrectEquals(User user, boolean gotCorrect);
    List<StudentAnswer> findByUserAndAttendedOnBetweenAndGotCorrectEquals(User user, Date from, Date to, boolean gotCorrect);
    List<StudentAnswer> countDistinctByAttendedOnBetweenAndUser(Date from, Date to, User user);
    List<StudentAnswer> findAllByUserAndAttendedOnEquals(User user, Date date);
    int countStudentAnswersByUserAndAttendedOn(User user, Date date);
    List<StudentAnswer> findByUserAndAttendedOn(User user, Date date);
    List<StudentAnswer> findByUserAndAttendedOnGreaterThanAndAttendedOnLessThanEqual(User user, Date from, Date to);
    //following twos are used for consistency services
    List<StudentAnswer> findAllByUserAndAttendedOnGreaterThanAndAttendedOnLessThanEqual(User user, Date from, Date to);
    List<StudentAnswer> findAllByUserAndAttendedOnGreaterThanAndAttendedOnLessThanEqualAndGotCorrectEquals(User user, Date from, Date to, boolean gotCorrect);
    //imporovement

}

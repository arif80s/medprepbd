package com.kids.crm.model;

import lombok.*;

import java.time.LocalDate;
import java.util.Comparator;
import java.util.Date;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class GraphStudentResult
{
    private LocalDate date;
    private long correct;
    private long wrong;
}
